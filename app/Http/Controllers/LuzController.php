<?php

namespace App\Http\Controllers;

use App\Models\luz;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Illuminate\Support\Facades\Http;

class LuzController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     * 
     */ public function historial(){
        return luz::all();
    }
    public function index()
    {
        $respuesta=HTTP::get('https://io.adafruit.com/api/v2/FabianOrtiz/feeds/luz/data/last?x-aio-key=aio_NZPb870hQ0iPSZ4Bj4lHSZvsai7C');
        $data=$respuesta->object();
        $model = new luz();
    
        $model->luz=$data->value;
       
        $model->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreluzRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreluzRequest $request)
    {
        luz::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'luz guardada'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\luz  $luz
     * @return \Illuminate\Http\Response
     */
    public function show(luz $luz)
    {
        return response()->json([
            'res'=>true,
            'luz'=>$luz
        ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\luz  $luz
     * @return \Illuminate\Http\Response
     */
    public function edit(luz $luz)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateluzRequest  $request
     * @param  \App\Models\luz  $luz
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateluzRequest $request, luz $luz)
    {
        $luz->update($luz->all());
        return response()->json([
            'res'=>true,
            'msg'=>'luz actualizada'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\luz  $luz
     * @return \Illuminate\Http\Response
     */
    public function destroy(luz $luz)
    {
        $luz->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'luz eliminada'
        ],200);
    }
}

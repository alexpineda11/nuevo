<?php

namespace App\Http\Controllers;

use App\Models\humedad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Illuminate\Support\Facades\Http;

class HumedadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     * 
     * 
     */public function historial(){
        return humedad::all();
    }


    public function index()
    {
        $respuesta=HTTP::get('https://io.adafruit.com/api/v2/FabianOrtiz/feeds/humedad/data/last?x-aio-key=aio_NZPb870hQ0iPSZ4Bj4lHSZvsai7C');
        $data=$respuesta->object();
            $model = new humedad();
        
            $model->humedad=$data->value;
           
            $model->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorehumedadRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorehumedadRequest $request)
    {
        humedad::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'humedad guardada'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\humedad  $humedad
     * @return \Illuminate\Http\Response
     */
    public function show(humedad $humedad)
    {
        return response()->json([
            'res'=>true,
            'humedad'=>$humedad
        ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\humedad  $humedad
     * @return \Illuminate\Http\Response
     */
    public function edit(humedad $humedad)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatehumedadRequest  $request
     * @param  \App\Models\humedad  $humedad
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatehumedadRequest $request, humedad $humedad)
    {
        $temperatura->update($temperatura->all());
        return response()->json([
            'res'=>true,
            'msg'=>'temperatura actualizada'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\humedad  $humedad
     * @return \Illuminate\Http\Response
     */
    public function destroy(humedad $humedad)
    {
        $humedad->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'humedad eliminada'
        ],200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\boton;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Log;
use Illuminate\Support\Facades\Http;


class BotonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function historial(){
        return boton::all();
    }


    public function index()
    {
        $respuesta=HTTP::get('https://io.adafruit.com/api/v2/FabianOrtiz/feeds/boton/data/last?x-aio-key=aio_NZPb870hQ0iPSZ4Bj4lHSZvsai7C');
        $data=$respuesta->object();
            $model = new boton();
        
            $model->boton=$data->value;
           
            $model->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorebotonRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorebotonRequest $request)
    {
        boton::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'boton guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\boton  $boton
     * @return \Illuminate\Http\Response
     */
    public function show(boton $boton)
    {
        return response()->json([
            'res'=>true,
            'boton'=>$boton
        ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\boton  $boton
     * @return \Illuminate\Http\Response
     */
    public function edit(boton $boton)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatebotonRequest  $request
     * @param  \App\Models\boton  $boton
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatebotonRequest $request, boton $boton)
    {
        $boton->update($boton->all());
        return response()->json([
            'res'=>true,
            'msg'=>'boton actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\boton  $boton
     * @return \Illuminate\Http\Response
     */
    public function destroy(boton $boton)
    {
        $boton->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'boton eliminado'
        ],200);
    }
}

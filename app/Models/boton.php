<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class boton extends Model
{
    protected $table ="botons";
     
    protected $fillable = [
    
        'boton',
        
    ];

    protected $primarykey='id';
    public function getJWTIdentifier()
    {
    	return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
    	return [];
    }
}

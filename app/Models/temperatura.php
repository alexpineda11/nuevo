<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class temperatura extends Model
{
    protected $table ="temperaturas";
     
    protected $fillable = [
    
        'temperatura',
        
    ];

    protected $primarykey='id';
    public function getJWTIdentifier()
    {
    	return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
    	return [];
    }
}
